export interface IRadiobutton {
    /** Id of the button. */
    readonly id: string;

    /** Name of the button. */
    readonly name: string;

    /** Description for the button. */
    readonly description?: string;

    /** Value of the button. */
    readonly value?: string;

    /** Score of the button. */
    readonly score?: number;
}
