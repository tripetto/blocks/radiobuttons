/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    Markdown,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    isBoolean,
    npgettext,
    pgettext,
    slots,
    supplies,
    tripetto,
} from "@tripetto/builder";
import { Radiobutton } from "./radiobutton";
import { RadiobuttonCondition } from "./conditions/radiobutton";
import { ScoreCondition } from "./conditions/score";
import { TScoreModes } from "../runner/conditions/score";

/** Assets */
import ICON from "../../assets/icon.svg";
import RADIOBUTTON from "../../assets/radiobutton.svg";
import ICON_SCORE from "../../assets/score.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:radiobuttons", "Radio buttons");
    },
})
export class Radiobuttons extends NodeBlock {
    radioSlot!: Slots.String;

    @definition("items")
    @affects("#label")
    @supplies<Radiobuttons>("#slot", "button")
    readonly buttons = Collection.of<Radiobutton, Radiobuttons>(
        Radiobutton,
        this
    );

    @definition("boolean", "optional")
    randomize?: boolean;

    get label() {
        return npgettext(
            "block:radiobuttons",
            "%2 (%1 option)",
            "%2 (%1 options)",
            this.buttons.count,
            this.type.label
        );
    }

    @slots
    defineSlots(): void {
        this.radioSlot = this.slots.static({
            type: Slots.String,
            reference: "button",
            label: pgettext("block:radiobuttons", "Selected button"),
            exchange: ["required", "alias", "exportable"],
        });
    }

    @editor
    defineEditor(): void {
        this.editor.groups.general();
        this.editor.name();
        this.editor.description();
        this.editor.explanation();

        const collection = this.editor.collection({
            collection: this.buttons,
            title: pgettext("block:radiobuttons", "Radio buttons"),
            icon: RADIOBUTTON,
            placeholder: pgettext("block:radiobuttons", "Unnamed radio button"),
            sorting: "manual",
            autoOpen: true,
            allowVariables: true,
            allowImport: true,
            allowExport: true,
            allowDedupe: true,
            showAliases: true,
            markdown:
                Markdown.MarkdownFeatures.Formatting |
                Markdown.MarkdownFeatures.Hyperlinks,
            emptyMessage: pgettext(
                "block:radiobuttons",
                "Click the + button to add a radio button..."
            ),
        });

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:radiobuttons", "Randomization"),
            form: {
                title: pgettext("block:radiobuttons", "Randomization"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:radiobuttons",
                            "Randomize the answers (using [Fisher-Yates shuffle](%1))",
                            "https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle"
                        ),
                        Forms.Checkbox.bind(this, "randomize", undefined, true)
                    ).markdown(),
                ],
            },
            activated: isBoolean(this.randomize),
        });

        this.editor.groups.options();
        this.editor.required(this.radioSlot);
        this.editor.visibility();

        this.editor.scores({
            target: this,
            collection,
            description: pgettext(
                "block:radiobuttons",
                "Generates a score based on the selected radio button. Open the settings panel for each button to set the score."
            ),
        });

        this.editor.alias(this.radioSlot);
        this.editor.exportable(this.radioSlot);
    }

    @conditions
    defineConditions(): void {
        this.buttons.each((button: Radiobutton) => {
            if (button.name) {
                this.conditions.template({
                    condition: RadiobuttonCondition,
                    markdown: button.name,
                    burst: "branch",
                    props: {
                        slot: this.radioSlot,
                        button: button,
                    },
                });
            }
        });

        const score = this.slots.select("score", "feature");

        if (score && score.label) {
            const group = this.conditions.group(
                score.label,
                ICON_SCORE,
                false,
                true
            );

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext(
                            "block:radiobuttons",
                            "Score is equal to"
                        ),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:radiobuttons",
                            "Score is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:radiobuttons",
                            "Score is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:radiobuttons",
                            "Score is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext(
                            "block:radiobuttons",
                            "Score is between"
                        ),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:radiobuttons",
                            "Score is not between"
                        ),
                    },
                    {
                        mode: "defined",
                        label: pgettext(
                            "block:radiobuttons",
                            "Score is calculated"
                        ),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:radiobuttons",
                            "Score is not calculated"
                        ),
                    },
                ],
                (condition: { mode: TScoreModes; label: string }) => {
                    group.template({
                        condition: ScoreCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: score,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }
    }
}
